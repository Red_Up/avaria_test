import 'dart:math';

import 'package:avaria_test/models/item.dart';

class RandomGenerator {
  var _random = Random();

  Item getRandomItem() {
    return items[_random.nextInt(items.length)];
  }

  List<Item> getItemList() {
    var minCount = 10000;
    var maxCount = 100000;

    var itemsCount = minCount + _random.nextInt(maxCount - minCount);

    return List.generate(itemsCount, (index) {
      return getRandomItem();
    });
  }
}
