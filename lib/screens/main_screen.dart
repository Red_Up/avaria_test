import 'package:avaria_test/bloc/goods_bloc.dart';
import 'package:avaria_test/widgets/grid_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainScreens extends StatelessWidget {
  const MainScreens({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ItemsBloc(),
      child: BlocBuilder<ItemsBloc, ItemsState>(
        builder: (context, state) {
          return Scaffold(
            backgroundColor: Colors.black,
            extendBodyBehindAppBar: false,
            appBar: AppBar(
              elevation: 0,
              actions: [
                IconButton(
                  splashColor: Colors.black,
                  splashRadius: 30,
                  icon: Icon(
                    Icons.add_box_sharp,
                    color: Colors.blueAccent,
                    size: 30,
                  ),
                  onPressed: () {
                    context.read<ItemsBloc>().add(AddItemEvent());
                  },
                ),
              ],
              backgroundColor: Colors.transparent,
              title: Text(
                "Длина списка: ${state.items?.length}",
              ),
            ),
            body: GridView.builder(
              padding: EdgeInsets.all(20.0),
              itemCount: state.items?.length ?? 0,
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
              ),
              itemBuilder: (BuildContext context, index) {
                var item = state.items![index];
                return GridItem(item: item);
              },
            ),
          );
        },
      ),
    );
  }
}
