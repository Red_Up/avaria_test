class Item {
  String name;
  String image;

  Item({required this.name, required this.image});
}

List items = [
  Item(
      name: "Banana",
      image:
          "https://st3.depositphotos.com/7597710/15931/i/600/depositphotos_159318140-stock-photo-yellow-banana-on-black.jpg"),
  Item(
      name: "Pineapple",
      image:
          "https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX1706670.jpg"),
  Item(
      name: "Mango",
      image:
          "https://thumbs.dreamstime.com/b/%D0%BC%D0%B0%D0%BD%D0%B3%D0%BE-%D0%BD%D0%B0-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B9-%D0%BF%D1%80%D0%B5-%D0%BF%D0%BE%D1%81%D1%8B-%D0%BA%D0%B5-81772698.jpg")
];
