import 'package:avaria_test/bloc/goods_bloc.dart';
import 'package:avaria_test/models/item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GridItem extends StatelessWidget {
  const GridItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  final Item item;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(top: 5, bottom: 5),
          alignment: Alignment.center,
          child: Column(
            children: [
              Expanded(
                child: Image.network(
                  item.image,
                  fit: BoxFit.fitWidth,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                item.name,
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
        Positioned(
          right: 0.0,
          top: 0,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                shape: CircleBorder(), primary: Colors.black),
            child: Container(
              width: 24,
              height: 24,
              alignment: Alignment.center,
              decoration: BoxDecoration(shape: BoxShape.circle),
              child: Icon(
                Icons.delete,
                color: Colors.blue,
              ),
            ),
            onPressed: () {
              context.read<ItemsBloc>().add(RemoveItemEvent(item));
            },
          ),
        ),
      ],
    );
  }
}
