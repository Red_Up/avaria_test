import 'dart:async';

import 'package:avaria_test/models/item.dart';
import 'package:avaria_test/repository/item_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'goods_event.dart';

part 'goods_state.dart';

class ItemsBloc extends Bloc<ItemsEvent, ItemsState> {
  final randomGenerator = RandomGenerator();

  ItemsBloc() : super(ItemsState()) {
    add(ItemsCreatedEvent(randomGenerator.getItemList()));
  }

  @override
  Stream<ItemsState> mapEventToState(
    ItemsEvent event,
  ) async* {
    switch (event.runtimeType) {
      case ItemsCreatedEvent:
        yield _itemsCreated(event as ItemsCreatedEvent);
        break;
      case AddItemEvent:
        yield _addItem();
        break;
      case RemoveItemEvent:
        yield _removeItem(event as RemoveItemEvent);
        break;
    }
  }

  ItemsState _itemsCreated(ItemsCreatedEvent event) {
    return state.copyWith(items: event.items);
  }

  ItemsState _addItem() {
    var newItem = randomGenerator.getRandomItem();

    List<Item> newList = List.of((state.items ?? []))..insert(0, newItem);

    return state.copyWith(items: newList);
  }

  ItemsState _removeItem(RemoveItemEvent event) {
    if (state.items != null) {
      List<Item> oldItems = this.state.items!;
      List<Item> newList = List.of(oldItems)..remove(event.removableItem);
      return state.copyWith(items: newList);
    }
    return state;
  }
}
