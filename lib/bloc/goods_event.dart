part of 'goods_bloc.dart';

@immutable
abstract class ItemsEvent extends Equatable {}

class ItemsCreatedEvent extends ItemsEvent {
  final List<Item> items;

  ItemsCreatedEvent(this.items);

  @override
  List<Object> get props => [items];
}

class AddItemEvent extends ItemsEvent {
  @override
  List<Object> get props => [];
}

class RemoveItemEvent extends ItemsEvent {
  final Item removableItem;

  RemoveItemEvent(this.removableItem);

  @override
  List<Object> get props => [removableItem];
}
