part of 'goods_bloc.dart';

class ItemsState extends Equatable {
  final List<Item>? items;

  ItemsState copyWith({
    List<Item>? items,
  }) {
    return new ItemsState(
      items: items ?? this.items,
    );
  }

  ItemsState({
    this.items,
  });

  @override
  List<Object?> get props => [
        items,
      ];
}
